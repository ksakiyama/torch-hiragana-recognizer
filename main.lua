require 'torch'
require 'cutorch'
require 'image'
require 'cunn'
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods

print("program starts.")

-- 10-class problem
noutputs = 71

-- input dimensions
nfeats = 1
width = 32
height = 32
ninputs = nfeats*width*height

-- number of hidden units (for MLP only):
nhiddens = ninputs / 2

-- hidden units, filter sizes (for ConvNet only):
nstates = {64,64,128}
filtsize = 5
poolsize = 2
normkernel = image.gaussian1D(7)

-- build DNN
local function build_model()

  -- 【実装したいやつ】
  local model = nn.Sequential()
  model:add(nn.SpatialConvolutionMM(1, 32, 3, 3))
  model:add(nn.ReLU())
  model:add(nn.SpatialConvolutionMM(32, 32, 3, 3))
  model:add(nn.ReLU())
  model:add(nn.SpatialMaxPooling(2, 2, 2, 2))
  model:add(nn.Dropout()) -- default 0.5

  model:add(nn.SpatialConvolutionMM(32, 64, 3, 3))
  model:add(nn.ReLU())
  model:add(nn.SpatialConvolutionMM(64, 64, 3, 3))
  model:add(nn.ReLU())
  model:add(nn.SpatialMaxPooling(2, 2, 2, 2))
  model:add(nn.Dropout())

  model:add(nn.View(64 * 5 * 5))  -- TODOなんで5x5になるんだっけ？
  model:add(nn.Linear(64 * 5 * 5, 256))
  model:add(nn.ReLU())
  model:add(nn.Dropout())
  model:add(nn.Linear(256, 71))
  model:add(nn.LogSoftMax())

  return model

  -- 【サンプル】
  -- local model = nn.Sequential()
  --
  -- -- stage 1 : filter bank -> squashing -> L2 pooling -> normalization
  -- model:add(nn.SpatialConvolutionMM(nfeats, nstates[1], filtsize, filtsize))
  -- model:add(nn.ReLU())
  -- model:add(nn.SpatialMaxPooling(poolsize,poolsize,poolsize,poolsize))
  --
  -- -- stage 2 : filter bank -> squashing -> L2 pooling -> normalization
  -- model:add(nn.SpatialConvolutionMM(nstates[1], nstates[2], filtsize, filtsize))
  -- model:add(nn.ReLU())
  -- model:add(nn.SpatialMaxPooling(poolsize,poolsize,poolsize,poolsize))
  --
  -- -- stage 3 : standard 2-layer neural network
  -- model:add(nn.View(nstates[2]*filtsize*filtsize))
  -- model:add(nn.Dropout(0.5))
  -- model:add(nn.Linear(nstates[2]*filtsize*filtsize, nstates[3]))
  -- model:add(nn.ReLU())
  -- model:add(nn.Linear(nstates[3], noutputs))
  --
  -- model:add(nn.LogSoftMax())
  -- return model
end

local function make_tensor(textfile)
  imgpaths = {}
  labels = {}

  -- read textfile
  local file = io.input(textfile)
  for line in file:lines() do
    local imgpath, label = unpack(line:split(" "))
    imgpaths[#imgpaths + 1] = imgpath
    labels[#labels + 1] = tonumber(label) + 1
  end

  -- define tensor
  labelTensor = torch.Tensor(#labels)
  imageTensor = torch.Tensor(#imgpaths, 1, 32, 32)

  for i = 1, #labels do
      labelTensor[i] = labels[i]
  end

  for i = 1, #imgpaths do
    local img = image.load(imgpaths[i], 1)
    imageTensor[i][1] = img
  end

  dataset = {
    data = imageTensor:float(),
    labels = labelTensor,
    size = #imgpaths
  }
  return dataset
end


--------------------------------------------------
-- global setting
--------------------------------------------------
torch.setdefaulttensortype('torch.FloatTensor')
torch.setnumthreads(2)
torch.manualSeed(1)

-- batch size
batchSize = 1024 * 2

plot = true

--------------------------------------------------
-- prepare data
--------------------------------------------------
trainData = make_tensor('traindata.txt')
testData = make_tensor('testdata.txt')

--------------------------------------------------
-- build network model
--------------------------------------------------
model = build_model()
model:cuda()

-- check
print(model)

--------------------------------------------------
-- criterion
--------------------------------------------------
criterion = nn.ClassNLLCriterion()
criterion:cuda()

--------------------------------------------------
-- confusion
--------------------------------------------------
classes = {}
for line in io.input('labels.txt'):lines() do
  classes[#classes + 1] = line
end

confusion = optim.ConfusionMatrix(classes)

trainLogger = optim.Logger(paths.concat('results', 'train.log'))
testLogger = optim.Logger(paths.concat('results', 'test.log'))

if model then
   parameters, gradParameters = model:getParameters()
end

optimState = {
   eta0 = 1e-3,
   t0 = trainData.size * 1
}
-- optimMethod = optim.asgd
-- optimMethod = optim.adam

--------------------------------------------------
-- train
--------------------------------------------------
function train(epoch)
  -- local vars
  local time = sys.clock()

  -- set model to training mode (for modules that differ in training and testing, like Dropout)
  model:training()

  -- shuffle at each epoch
  shuffle = torch.randperm(trainData.size)

  -- do one epoch
  print('==> doing epoch on training data:')
  print("==> online epoch # " .. epoch .. ' [batchSize = ' .. batchSize .. ']')
  for t = 1, trainData.size, batchSize do
    -- disp progress
    xlua.progress(t, trainData.size)

    -- create mini batch
    local inputs = {}
    local targets = {}
    for i = t,math.min(t + batchSize - 1, trainData.size) do
      -- load new sample
      local input = trainData.data[shuffle[i]]
      local target = trainData.labels[shuffle[i]]

      input = input:cuda();

      table.insert(inputs, input)
      table.insert(targets, target)
    end

    -- create closure to evaluate f(X) and df/dX
    local feval = function(x)
                    -- get new parameters
                    if x ~= parameters then
                      parameters:copy(x)
                    end

                    -- reset gradients
                    gradParameters:zero()

                    -- f is the average of all criterions
                    local f = 0

                    -- evaluate function for complete mini batch
                    for i = 1,#inputs do
                      -- estimate f
                      local output = model:forward(inputs[i])
                      local err = criterion:forward(output, targets[i])
                      f = f + err

                      -- estimate df/dW
                      local df_do = criterion:backward(output, targets[i])
                      model:backward(inputs[i], df_do)

                      -- update confusion
                      confusion:add(output, targets[i])
                    end

                    -- normalize gradients and f(X)
                    gradParameters:div(#inputs)
                    f = f/#inputs

                    -- return f and df/dX
                    return f,gradParameters
                  end

    -- optimize on current mini-batch
    -- _,_,average = optimMethod(feval, parameters, optimState)
    optim.adam(feval, parameters)

  end

  -- time taken
  time = sys.clock() - time
  time = time / trainData.size
  print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')

  -- print confusion matrix
  print(confusion)

  -- update logger/plot
  trainLogger:add{['% mean class accuracy (train set)'] = confusion.totalValid * 100}
  if plot then
    trainLogger:style{['% mean class accuracy (train set)'] = '-'}
    trainLogger:plot()
  end

  -- save/log current net
  local filename = paths.concat('results', 'model.net')
  os.execute('mkdir -p ' .. sys.dirname(filename))
  print('==> saving model to '..filename)
  torch.save(filename, model)

  -- next epoch
  confusion:zero()
end

--------------------------------------------------
-- test
--------------------------------------------------
function test()
  -- local vars
  local time = sys.clock()

  -- averaged param use?
  if average then
    cachedparams = parameters:clone()
    parameters:copy(average)
  end

  -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
  model:evaluate()

  -- test over test data
  print('==> testing on test set:')
  for t = 1,testData.size do
    -- disp progress
    xlua.progress(t, testData.size)

    -- get new sample
    local input = testData.data[t]
    input = input:cuda()
    local target = testData.labels[t]

    -- test sample
    local pred = model:forward(input)
    confusion:add(pred, target)
  end

  -- timing
  time = sys.clock() - time
  time = time / testData.size
  print("\n==> time to test 1 sample = " .. (time*1000) .. 'ms')

  -- print confusion matrix
  print(confusion)

  -- update log/plot
  testLogger:add{['% mean class accuracy (test set)'] = confusion.totalValid * 100}
  if plot then
    testLogger:style{['% mean class accuracy (test set)'] = '-'}
    testLogger:plot()
  end

  -- averaged param use?
  if average then
    -- restore parameters
    parameters:copy(cachedparams)
  end

  -- next iteration:
  confusion:zero()
end

--------------------------------------------------
-- run
--------------------------------------------------
for i = 1, 500 do
  train(i)
  test()
end

print("program ends.")
